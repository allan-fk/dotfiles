sudo apt-get update
sudo apt-get updgrade
sudo apt-get install zsh
sudo apt-get install vim
sudo apt-get install git
sudo apt-get install curl


git submodule update --init 

HOME=~
FILES='zshrc'   
for f in $FILES;
do ln -s $(pwd)/$f $HOME/.$f
done;


#NVM
echo "===========> INSTALL NVM"
sh nvm/install.sh
echo "===========> IF NVM NOT WORK ADD THIS LINE TO .zshrc"
[[ -s $HOME/.nvm/nvm.sh ]] && . $HOME/.nvm/nvm.sh  # This loads NVM

nvm install 10.16.0
nvm alias default 10.16.0

#Yarn
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt-get install --no-install-recommends yarn

#POWERLINE
echo "===========> INSTALL FONTS"
chmod +x ./fonts/install.sh
./fonts/install.sh
echo "Don't forget to change font for powerline font"

# Mettre /bin/zsh dans la ligne allan dans /etc/passwd



# DOCKER
## install prerequisite packages
sudo apt install apt-transport-https ca-certificates curl gnupg2 software-properties-common
## add the GPG key for the official Docker repository in system
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
## Add the Docker repository to APT sources
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
## Update the package database
sudo apt update
#### For make sure install from the Docker repo instead of the default Debian repo:
#### Conditions à faire
##### apt-cache policy docker-ce
### Install Docker
sudo apt install docker-ce
### Check if is running
#### Conditions à faire
### sudo systemctl status docker

### Activate docker pour les users non admins
##  Create the docker group
sudo groupadd docker
## Add your user to the docker group.
sudo usermod -aG docker $USER
echo "Logout and login again after run 'docker run hello-world' :)"
## docker run hello-world

# DOCKER-COMPOSE
## download the current stable release of Docker Compose:
sudo curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
## Apply executable permissions to the binary:
sudo chmod +x /usr/local/bin/docker-compose
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose


# MONGODB
## Install missing package
sudo apt install dirmngr
## Import the public key used by the package management system
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 9DA31620334BD75D9DCB49F368818C72E52529D4
## Create a sources.list file for MongoDB
## If Debian 8
## echo "deb http://repo.mongodb.org/apt/debian jessie/mongodb-org/4.0 main" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.0.list
echo "deb http://repo.mongodb.org/apt/debian stretch/mongodb-org/4.0 main" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.0.list
sudo apt-get update


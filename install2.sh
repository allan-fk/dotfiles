#!/bin/bash
# Bash Menu Script Example

waiting()
{
chars="/-\|"
y=0
while [ $y != 5 ]; do
  for (( i=0; i<${#chars}; i++ )); do
    sleep 0.05
    echo -en "${chars:$i:1}" "\r"
  done
let "y++"
done
}

PS3='Please enter your OS: '
options=("1:Ubuntu" "2:Debian" "3:Exit")
select opt in "${options[@]}"
do
    case $opt in
        "1:Ubuntu")
            echo "you chose choice Ubuntu"
	    break
            ;;
        "2:Debian")
            echo "you chose choice Debian"
	    break
            ;;
        "3:Exit")
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done

### INIT ###
sudo apt-get update
sudo apt-get updgrade
sudo apt-get install zsh
sudo apt-get install vim
sudo apt-get install git
sudo apt-get install curl


### NVM ###
target='./nvm'
if [ -d "$target" ]; then
	if find "$target" -mindepth 1 -print -quit 2>/dev/null | grep -q .; then
	    break;
	else
        echo   "===========> INSTALL NVM"
        waiting
        git submodule update --init 
	sh nvm/install.sh
	# echo "[[ -s $HOME/.nvm/nvm.sh ]] && . $HOME/.nvm/nvm.sh" >> ~/.bash_profile
        echo "===========> IF NVM NOT WORK ADD THIS LINE TO .zshrc"
        echo '[[ -s $HOME/.nvm/nvm.sh ]] && . $HOME/.nvm/nvm.sh'
        while true; do
            read -p 'Yes ?' yn
            case $yn in
                [Yy]* ) break;;
                * ) echo "Please answer yes.";;
            esac
        done
        [[ -s $HOME/.nvm/nvm.sh ]] && . $HOME/.nvm/nvm.sh  # This loads NVM
        nvm install 10.16.0
        nvm alias default 10.16.0
	fi
fi


### YARN ###
echo   "===========> INSTALL YARN"
waiting
sudo apt remove cmdtest
sudo apt remove yarn
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt-get update && sudo apt-get install yarn
yarn global add npm

### MONGODB ###
echo   "===========> INSTALL MONGODEB"
waiting
sudo apt install dirmngr
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 9DA31620334BD75D9DCB49F368818C72E52529D4
if [ $REPLY = 1 ]
then
    ## if ubuntu 16.04
    ## echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/4.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.0.list
    echo "deb [ arch=amd64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.0.list
elif [ $REPLY = 2 ]
then
	## If Debian 8
    ## echo "deb http://repo.mongodb.org/apt/debian jessie/mongodb-org/4.0 main" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.0.list
    echo "deb http://repo.mongodb.org/apt/debian stretch/mongodb-org/4.0 main" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.0.list
fi
sudo apt-get update
sudo apt-get install -y mongodb-org

### DOCKER ###
echo   "===========> INSTALL DOCKER"
waiting
sudo apt-get remove docker docker-engine docker.io containerd runc
sudo apt install apt-transport-https ca-certificates curl gnupg2 software-properties-common
if [ $REPLY = 1 ]
then
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
elif [ $REPLY = 2 ]
then
	curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
fi
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io

### DOCKER-COMPOSE ###
echo   "===========> INSTALL DOKER-COMPOSE"
waiting
sudo curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose

### DOCKER TO SUDOERS ###
echo   "===========> ADD DOCKER TO SUDOERS"
waiting
sudo groupadd docker
sudo usermod -aG docker $USER
echo "Logout and login again after run 'docker run hello-world' :)"
while true; do
    read -p 'Yes ?' yn
    case $yn in
        [Yy]* ) break;;
        * ) echo "Please answer yes.";;
    esac
done
